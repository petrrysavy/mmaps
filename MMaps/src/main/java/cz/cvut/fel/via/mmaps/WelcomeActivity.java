package cz.cvut.fel.via.mmaps;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import cz.cvut.fel.via.mmaps.authentication.AuthenticationManager;

public class WelcomeActivity extends Activity {

    public static WelcomeActivity instance;

    public WelcomeActivity()
    {
        if(instance == null)
            instance = this;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        AuthenticationManager am = AuthenticationManager.getInstance();
        am.setContext(this);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

        // if the user is logged, then redirect directly to Activity with memory maps
        // TODO redirect to correct activity when user is already logged
        if(AuthenticationManager.getInstance().isUserLogged())
        {
            // just temporary test method
            /*if(AuthenticationManager.getInstance().getLoggedUser().getName().equals("petr"))
                new Thread()
                {
                    public void run()
                    {
                try {
                    AuthenticationManager.getInstance().logoutUser();
                    Log.i("Welcome", "logged out");
                } catch (JSONException e) {
                    Log.i("Welcome", "logged out error", e);
                    e.printStackTrace();
                } catch (IOException e) {
                    Log.i("Welcome", "logged out error", e);
                    e.printStackTrace();
                } catch (AuthenticationException e) {
                    Log.i("Welcome", "logged out error", e);
                    e.printStackTrace();
                }}
                }.start();
            else
            {*/
            finish();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            //}
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void signIn(View view)
    {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }

    public void signUp(View view)
    {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_welcome, container, false);
            return rootView;
        }
    }

}
