package cz.cvut.fel.via.mmaps.connection;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import cz.cvut.fel.via.mmaps.authentication.AuthenticationException;
import cz.cvut.fel.via.mmaps.authentication.AuthenticationManager;

/**
 * Singleton that handles connection with the server.
 *
 * Created by Petr on 24.11.13.
 */
public class ServerConnectionManager {
    private static final ServerConnectionManager instance = new ServerConnectionManager("http://46.255.228.224");

    /** The server address. */
    private final String address;

    private ServerConnectionManager(String address) {
        this.address = address;
    }

    public static ServerConnectionManager getInstance() {
        return instance;
    }

    /** Sends the given request to the server. */
    public HttpResponse makeRequest(HttpUriRequest request) throws IOException
    {
        HttpClient httpclient = new DefaultHttpClient();
        return httpclient.execute(request);
     }

    /** Creates and PUT request with the given body.
     *
     * @param target The target address.
     * @param body The body of the request. May be null.
     * @return The new request.
     */
    public HttpUriRequest createPut(String target, String body)
    {
        Log.i("ServerConnectionManager", body);

        HttpPut put = new HttpPut(address + target);
        if(body != null)
        {
            StringEntity entity = null;
            try {
                entity = new StringEntity(body);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            entity.setContentType("application/json;charset=UTF-8");//text/plain;charset=UTF-8
            entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json;charset=UTF-8"));
            put.setEntity(entity);
        }

        return put;
    }

    /** Creates and POST request with the given body.
     *
     * @param target The target address.
     * @param body The body of the request. May be null.
     * @return The new request.
     */
    public HttpUriRequest createPost(String target, String body)
    {
        HttpPost post = new HttpPost(address + target);
        if(body != null)
        {
            StringEntity entity = null;
            try {
                entity = new StringEntity(body);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            entity.setContentType("application/json;charset=UTF-8");//text/plain;charset=UTF-8
            entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json;charset=UTF-8"));
            post.setEntity(entity);
        }

        return post;
    }

    /** Creates and GET request.
     *
     * @param target The target address.
     * @return The new request.
     */
    public HttpUriRequest createGet(String target)
    {
        HttpGet get = new HttpGet(address + target);
        return get;
    }
    
    /** Creates DELETE request.
     *
     * @param target The target address.
     * @return The new request.
     */
    public HttpUriRequest createDelete(String target)
    {
        HttpDelete get = new HttpDelete(address + target);
        return get;
    }

    /**
     * Parses JSON request body from the response.
     * @param response Response.
     * @return The JSON body of the response.
     * @throws IOException Some IO exception.
     * @throws JSONException The body does not contain valid JSON.
     */
    public static JSONObject getJSONBody(HttpResponse response) throws IOException, JSONException {
        HttpEntity ent = response.getEntity();
        InputStream is = ent.getContent();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }
        is.close();
        return new JSONObject(sb.toString());
    }

    /**
     * Prepares JSONStringer that shall be used within the authenticated request.
     * @return The JSONStringer.
     * @throws JSONException Some exception.
     */
    public JSONStringer prepareJSON() throws JSONException {
        return new JSONStringer().object();
    }

    /**
     * Sends an authorised request to the server.
     *
     * @param target target address
     * @param method "PUT" for put method, "POST" for post method
     * @param data JSON stringer with unclosed object. Use the JSONStringer given by the prepareJSON method. See example in wiki.
     * @return The server response.
     * @throws AuthenticationException Something is wrong with the credentials. Maybe there is no user logged into the application.
     */
    public synchronized HttpResponse makeAuthenticatedRequest(String target, String method, JSONStringer data) throws AuthenticationException, JSONException, IOException {
        AuthenticationManager am = AuthenticationManager.getInstance();
        if(!am.isUserLogged())
            throw new AuthenticationException("User is not logged.");
        
        String body = data == null ? "" : data.endObject().toString();
        
        HttpUriRequest request = null;
        if(method.equals("PUT"))
            request = createPut(target, body);
        else if(method.equals("POST"))
            request = createPost(target, body);
        else if(method.equals("GET"))
            request = createGet(target);
        else if(method.equals("DELETE"))
            request = createDelete(target);
        else
            throw new IllegalArgumentException("Unknown method : "+method);
            
        request.addHeader("X-Auth-Login", am.getLoggedUser().getName());
        request.addHeader("X-Auth-Token", am.sha256(String.format("%s %s %s %s", method, target, body, am.getLoggedUser().getHash())));

        return makeRequest(request);
    }
}
