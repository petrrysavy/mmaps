package cz.cvut.fel.via.mmaps.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by tomas on 10/28/13.
 */
public class NewNodeDialog extends DialogFragment {

    private String value;
    private EditText input = null;
    private long parentId;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Create new node");
        builder.setMessage("Input the name of new node:");
        input = new EditText(getActivity().getApplicationContext());
        input.setTextColor(Color.parseColor("#000000"));
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog,int whichButton){
                mListener.onDialogPositiveClick(NewNodeDialog.this);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
              public void onClick(DialogInterface dialog,int whichButton){
                mListener.onDialogNegativeClick(NewNodeDialog.this);
              }
        });
        return builder.create();
    }

    public NewNodeDialog(long parentId){
        this.parentId = parentId;
    }

    public String getNewName(){
        value = input.getText().toString();
        return value;
    }

    public long getParentId(){
        return parentId;
    }

    public interface NewNodeDialogListener{
        public void onDialogPositiveClick(NewNodeDialog dialog);
        public void onDialogNegativeClick(NewNodeDialog dialog);
    }

    NewNodeDialogListener mListener;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            mListener = (NewNodeDialogListener) activity;
        }catch(ClassCastException e){
            Log.e("chyba pri prenosu z dialogu","chyba pri prenosu z dialogu");
        }
    }

}
