package cz.cvut.fel.via.mmaps.utils;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

import cz.cvut.fel.via.mmaps.database.NodeDataSource;
import cz.cvut.fel.via.mmaps.utils.graphBuilder.GraphBuilder;

/**
 * Created by tomas on 10/23/13.
 */
public class CustomDrawableView extends View {


    private static final int FONT_SIZE = 28;
    private final int WIDTH = 160;
    private final int HEIGHT = 80;
    private NodeDataSource nodes;
    private ArrayList<ShapeDrawable> listOfShapes;
    private ArrayList<GraphNode> listOfNodes;
    private GraphNode graphRoot;
    private ShapeDrawable mDrawable;
    private Context context;
    private FragmentManager fm;
    private int[] colors;
    Random r;
    int x;
    int y;

    public CustomDrawableView(Context context,FragmentManager fm,NodeDataSource nodes) {
        super(context);
        this.context = context;
        this.fm = fm;
        this.nodes = nodes;
        r = new Random();
        colors = new int[]{Color.CYAN,Color.GREEN,Color.MAGENTA,Color.YELLOW};
        listOfShapes = new ArrayList<ShapeDrawable>();
        listOfNodes = new ArrayList<GraphNode>();
        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display d = wm.getDefaultDisplay();
        Point size = new Point();
        d.getSize(size);
        x = size.x;
        y = size.y;
        setWillNotDraw(false);
        requestLayout();
        invalidate();
    }

    public void drawEllipse(){
        int x = r.nextInt((int) (this.x*(2/3.0)));
        int y = r.nextInt((int) (this.y*(2/3.0)));
        int width = r.nextInt((int) (this.x*(1/3.0)));
        int height = r.nextInt((int) (this.y*(1/3.0)));

        int color = r.nextInt(colors.length);

        mDrawable = new ShapeDrawable(new OvalShape());
        mDrawable.getPaint().setColor(colors[color]);
        mDrawable.setBounds(x, y, x + width, y + height);
        listOfShapes.add(mDrawable);
    }

    public void drawEllipse(String name){
        int x = r.nextInt((int) (this.x*(2/3.0)));
        int y = r.nextInt((int) (this.y*(2/3.0)));
        int width = r.nextInt((int) (this.x*(1/3.0)));
        int height = r.nextInt((int) (this.y*(1/3.0)));

        int color = r.nextInt(colors.length);

        mDrawable = new ShapeDrawable(new OvalShape());
        mDrawable.getPaint().setColor(colors[color]);
        mDrawable.setBounds(x, y, x + width, y + height);
        listOfShapes.add(mDrawable);
    }

    public void drawEllipse(int x,int y,String name){
        int width = r.nextInt((int) (this.x*(1/3.0)));
        int height = r.nextInt((int) (this.y*(1/3.0)));

        int color = r.nextInt(colors.length);

        mDrawable = new ShapeDrawable(new OvalShape());
        mDrawable.getPaint().setColor(colors[color]);
        mDrawable.setBounds(x, y, x + width, y + height);
        listOfShapes.add(mDrawable);
    }

    public void drawGraph(GraphNode root){
        // at first calculate positions of the nodes
        //TODO this is probably not the best way
        listOfShapes.clear();
        listOfNodes.clear();
        Paint p = new Paint();
        p.setTextSize(FONT_SIZE);
        GraphBuilder gb = new GraphBuilder(p);
        gb.buildGraph(root);

        Queue<GraphNode> graph = new LinkedList<GraphNode>();
        graph.add(root);
        GraphNode g;
        while(!graph.isEmpty()){
            g = graph.poll();
            //int x = g.getLayer()*200;
            final Rect bounds = g.getBounds();
            int x = bounds.left;
            //int y = (g.getParent()!=null) ? g.getParent().getChildren().indexOf(g)*200 : 0;
            int y = bounds.top;
            //g.setPositions(new Point(x,y));
            //int width = WIDTH;
            //int height = HEIGHT;
            //int width = bounds.width();
            //int height = bounds.height();

            int color = r.nextInt(colors.length);
            mDrawable = new ShapeDrawable(new OvalShape());
            mDrawable.getPaint().setColor(colors[color]);
            //mDrawable.setBounds(x, y, x + width, y + height);
            mDrawable.setBounds(bounds);
            listOfShapes.add(mDrawable);
            listOfNodes.add(g);
            graph.addAll(g.getChildren());
        }
        invalidate();

    }

    public void drawRectangle(){
        int x = r.nextInt((int) (this.x*(2/3.0)));
        int y = r.nextInt((int) (this.y*(2/3.0)));
        int width = r.nextInt((int) (this.x*(1/3.0)));
        int height = r.nextInt((int) (this.y*(1/3.0)));

        int color = r.nextInt(colors.length);

        mDrawable = new ShapeDrawable(new RectShape());
        mDrawable.getPaint().setColor(colors[color]);
        mDrawable.setBounds(x, y, x + width, y + height);
        listOfShapes.add(mDrawable);
    }

    public void drawSquare(){
        int x = r.nextInt((int) (this.x*(2/3.0)));
        int y = r.nextInt((int) (this.y*(2/3.0)));
        int width = r.nextInt((int) (this.x*(1/3.0)));

        int color = r.nextInt(colors.length);

        mDrawable = new ShapeDrawable(new OvalShape());
        mDrawable.getPaint().setColor(colors[color]);
        mDrawable.setBounds(x, y, x + width, y + width);
        listOfShapes.add(mDrawable);
    }

    public void drawText(){
    }

    public void clearScreen(){
        listOfShapes.clear();
    }

    protected void onDraw(Canvas canvas) {
        ShapeDrawable s;
        String name;
        //Point p;
        Rect r;
        for(int i=0;i<listOfShapes.size();i++){
            r = listOfNodes.get(i).getBounds();

            // draw edges
            if (null != listOfNodes.get(i).getParent()){
                Rect ro2 = listOfNodes.get(i).getParent().getBounds();
                Paint p3 = new Paint();
                p3.setColor(Color.BLACK);
                p3.setStrokeWidth(3);
                canvas.drawLine(ro2.centerX(),ro2.centerY(),r.left - GraphBuilder.NODE_HORIZONTAL_DIST,r.centerY(),p3);
                canvas.drawLine(r.left - GraphBuilder.NODE_HORIZONTAL_DIST,r.centerY(), r.centerX(), r.centerY(), p3);
            }
        }

        for(int i=0;i<listOfShapes.size();i++) {
            Paint p2 = new Paint();
            p2.setColor(Color.BLACK);
            p2.setTextSize(FONT_SIZE);
            s = listOfShapes.get(i);
            name = listOfNodes.get(i).getName();
            r = listOfNodes.get(i).getBounds();

            // draw shape
            s.draw(canvas);

            // draw text
            canvas.drawText(name,r.left + GraphBuilder.HORIZONTAL_PADDING,r.bottom-GraphBuilder.VERTICAL_PADDING,p2);
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Log.v("GraphView", "On onMeasure==>");
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
        int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
        Log.v("GraphView",parentHeight+"----"+parentWidth);
        parentWidth = (parentWidth==0) ? this.x : parentWidth;
        setMeasuredDimension(parentWidth * 2, parentHeight);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event){
        double x = event.getX();
        double y = event.getY();
        Log.v("GraphView",x+"-------"+y);
        for(GraphNode gn:listOfNodes){
            //that's not really nice
            Rect r = gn.getBounds();
            if(r.contains((int)x, (int)y))
            {
                NodeDialog nd = new NodeDialog(context,fm,gn.getId(),gn.getName(),nodes);
                nd.show(fm,"Nic");
                return false;
            }

//            Point p = gn.getPositions();
//            if(x < p.x+HEIGHT && x > p.x && y<p.y+WIDTH && y>p.y){
//                NodeDialog nd = new NodeDialog(gn.getName());
//                nd.show(fm,"Nic");
//                return false;
//            }
        }
        return false;
    }

}


