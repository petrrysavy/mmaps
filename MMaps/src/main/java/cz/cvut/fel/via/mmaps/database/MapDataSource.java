package cz.cvut.fel.via.mmaps.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import cz.cvut.fel.via.mmaps.utils.GraphNode;

/**
 * Created by tomas on 5.12.13.
 */
public class MapDataSource {

    private SQLiteDatabase db;
    private MMapsSQLiteHelper dbHelper;
    private NodeDataSource nodes;
    private  String[] allColumns = {MMapsSQLiteHelper.COLUMN_MAP_ID,MMapsSQLiteHelper.COLUMN_MAP_NAME,MMapsSQLiteHelper.COLUMN_MAP_USER_ID};

    public MapDataSource(Context context){
        dbHelper = new MMapsSQLiteHelper(context);
        nodes = new NodeDataSource(context);
        nodes.open();
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Map createMap(String name, long mapId){
        Map tryLocal = getMap(mapId);
        if (tryLocal!=null){
            return tryLocal;
        }
        ContentValues values = new ContentValues();
        values.put(MMapsSQLiteHelper.COLUMN_MAP_NAME,name);
        values.put(MMapsSQLiteHelper.COLUMN_MAP_USER_ID,mapId);
        long insertID = db.insert(MMapsSQLiteHelper.TABLE_MAPS,null,values);
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_MAPS,allColumns,MMapsSQLiteHelper.COLUMN_MAP_ID + " = " + insertID,null,null,null,null);
        cursor.moveToFirst();
        Map newMap = cursorToMap(cursor);
        cursor.close();
        nodes.createNode("start","",insertID,-1);
        return newMap;
    }

    public Map createMap(String name, String url){
        Map tryLocal = getMap(url);
        if (tryLocal!=null){
            return tryLocal;
        }
        ContentValues values = new ContentValues();
        values.put(MMapsSQLiteHelper.COLUMN_MAP_NAME,name);
        values.put(MMapsSQLiteHelper.COLUMN_MAP_URL,url);
        long insertID = db.insert(MMapsSQLiteHelper.TABLE_MAPS,null,values);
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_MAPS,allColumns,MMapsSQLiteHelper.COLUMN_MAP_ID + " = " + insertID,null,null,null,null);
        cursor.moveToFirst();
        Map newMap = cursorToMap(cursor);
        cursor.close();
        nodes.createNode("start","",insertID,-1);
        return newMap;
    }

    public Map getMap(long mapId){
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_MAPS,allColumns,
                MMapsSQLiteHelper.COLUMN_MAP_ID + " = " + mapId,null,null,null,null);
        cursor.moveToFirst();
        if(cursor!=null && cursor.getCount()>0){
            return cursorToMap(cursor);
        }else{
            return null;
        }
    }

    public Map getMap(String url){
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_MAPS,allColumns,
                MMapsSQLiteHelper.COLUMN_MAP_URL + "=?",new String[]{url},null,null,null,null);
        if(cursor!=null && cursor.getCount()>0){
            cursor.moveToFirst();
            return cursorToMap(cursor);
        }else{
            return null;
        }
    }

    public int updateMap(Node newNode, String url) {
        ContentValues values = new ContentValues();
        values.put(MMapsSQLiteHelper.COLUMN_NODE_NAME,newNode.getName());
        values.put(MMapsSQLiteHelper.COLUMN_NODE_ID,newNode.getId());
        values.put(MMapsSQLiteHelper.COLUMN_NODE_MAP_ID, newNode.getMap_id());
        values.put(MMapsSQLiteHelper.COLUMN_NODE_PARENT_ID,newNode.getParent_id());
        values.put(MMapsSQLiteHelper.COLUMN_NODE_URL,url);
        int result = db.update(MMapsSQLiteHelper.TABLE_NODES, values,
                MMapsSQLiteHelper.COLUMN_NODE_ID + "=?", new String[]{newNode.getId() + ""});
        return result;
    }

    public int updateMap(Map map,String url){
        ContentValues values = new ContentValues();
        values.put(MMapsSQLiteHelper.COLUMN_MAP_NAME,map.getName());
        values.put(MMapsSQLiteHelper.COLUMN_MAP_ID,map.getId());
        values.put(MMapsSQLiteHelper.COLUMN_MAP_USER_ID,map.getId());
        values.put(MMapsSQLiteHelper.COLUMN_MAP_URL,url);
        int result = db.update(MMapsSQLiteHelper.TABLE_MAPS, values,
                MMapsSQLiteHelper.COLUMN_MAP_ID + "=?", new String[]{map.getId() + ""});
        return result;
    }

    public GraphNode buildGraphFromMapTable(long mapId){
        ArrayList<Node> no = (ArrayList<Node>) nodes.getNodes(mapId,-1);
        Queue<Node> queue = new ArrayDeque<Node>();
        Queue<GraphNode> queue2 = new ArrayDeque<GraphNode>();
        queue.add(no.get(0));
        GraphNode root = new GraphNode(no.get(0).getId(),no.get(0).getName());
        queue2.add(root);
        GraphNode current;
        Node n;
        ArrayList<Node> tempNodes;
        while(!queue.isEmpty()){
            n = queue.poll();
            current = queue2.poll();
            tempNodes = (ArrayList<Node>) nodes.getNodes(mapId,n.getId());
            /*if (n.getParent_id() >= 0){
                current = new GraphNode(n.getId(),n.getName());
            }else{
                current = root;
            }*/
            for(Node tN:tempNodes){
                GraphNode newGN = new GraphNode(tN.getId(), tN.getName());
                newGN.setParent(current);
                current.addChild(newGN);
                queue.add(tN);
                queue2.add(newGN);
            }
        }
        return root;
    }

    public void deleteMap(Map map){
        long id = map.getId();
        System.out.println("Deleting map with id: "+id);
        db.delete(MMapsSQLiteHelper.TABLE_MAPS,MMapsSQLiteHelper.COLUMN_MAP_ID + " = " + id,null);
    }

    public List<Map> getAllMaps() {
        List<Map> maps = new ArrayList<Map>();
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_MAPS,allColumns,null,null,null,null,null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            Map map = cursorToMap(cursor);
            maps.add(map);
            cursor.moveToNext();
        }
        cursor.close();
        return maps;
    }

    public int getMapsCount(long userId){
        Cursor c = db.rawQuery("SELECT count(*) FROM "+MMapsSQLiteHelper.TABLE_MAPS+" WHERE "
        +MMapsSQLiteHelper.COLUMN_MAP_USER_ID+"="+userId,null);
        c.moveToFirst();
        return c.getInt(0);
    }

    private Map cursorToMap(Cursor cursor){
        Map map = new Map();
        map.setId(cursor.getLong(0));
        map.setName(cursor.getString(1));
        map.setUser_id(cursor.getLong(2));
        if(!cursor.isNull(3)){
            map.setUrl(cursor.getString(3));
        }
        return map;
    }

}
