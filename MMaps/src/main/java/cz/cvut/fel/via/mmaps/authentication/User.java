package cz.cvut.fel.via.mmaps.authentication;

import android.content.SharedPreferences;

/**
 * Created by Petr on 24.11.13.
 */
public class User {
    public static final String NAME_PREF = "name";
    public static final String PASSWORD_HASH_PREF = "passwordHash";
    public static final String DUMMY_VALUE = "";
    private final String name;
    private final String passwordHashWithSalt;


    public User(String name, String password, String salt) {
        this.name = name;
        AuthenticationManager am =AuthenticationManager.getInstance();
        this.passwordHashWithSalt = am.sha256(password + salt);
    }

    /**
     * All three values are encoded with the AuthenticationManager encode method. The decode method is used.
     * @param name
     * @param passwordHashWithSalt
     */
    private User(String name, String passwordHashWithSalt) {
        if (name == null || passwordHashWithSalt == null || name.equals(DUMMY_VALUE) || passwordHashWithSalt.equals(DUMMY_VALUE))
            throw new IllegalArgumentException();
        this.name = AuthenticationManager.decode(name);
        this.passwordHashWithSalt = AuthenticationManager.decode(passwordHashWithSalt);
    }

    public String getName() {
        return name;
    }
    
    public String getHash() {
        return passwordHashWithSalt;
    }

    void storeCredential(SharedPreferences sp)
    {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(NAME_PREF, AuthenticationManager.encode(name));
        editor.putString(PASSWORD_HASH_PREF, AuthenticationManager.encode(passwordHashWithSalt));
        editor.commit();
    }

    protected static void clearCredentials(SharedPreferences sp)
    {
        AuthenticationManager am = AuthenticationManager.getInstance();
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(NAME_PREF, DUMMY_VALUE);
        editor.putString(PASSWORD_HASH_PREF, DUMMY_VALUE);
        editor.commit();
    }

    protected static User loadCredentials(SharedPreferences sp)
    {
        try{
        return new User(sp.getString(NAME_PREF, DUMMY_VALUE),
                sp.getString(PASSWORD_HASH_PREF, DUMMY_VALUE));
        }
        catch (IllegalArgumentException e){
            return null;        }

    }
}
