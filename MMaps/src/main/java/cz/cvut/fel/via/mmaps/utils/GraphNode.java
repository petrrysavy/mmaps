package cz.cvut.fel.via.mmaps.utils;

import android.graphics.Point;
import android.graphics.Rect;

import java.util.ArrayList;

/**
 * Created by tomas on 10/27/13.
 */
public class GraphNode {

    private String name;
    private long id;
    private GraphNode parent;
    private int layer;
    private ArrayList<GraphNode> children;
    private Rect bounds;

    public static GraphNode createGraph(){
        GraphNode root = new GraphNode(0,"root");
        root.setLayer(0);
        root.setParent(null);
        GraphNode actual;
        actual = root;
        GraphNode child;
        for(int i=1;i<5;i++){
        for(int j = 0; j <= 1 || Math.random() < 0.5; j++)
            {
            child = new GraphNode(i,"root_ch_"+j+" "+i);
            child.setParent(actual);
            child.setLayer(child.getParent().getLayer()+1);
            actual.addChild(child);
            }
            if(actual.getChildren().size() != 0)
                actual = actual.getChildren().get((int)Math.random() * actual.getChildren().size());
        }
        return root;
    }

    public GraphNode(long id,String name){
        this.id = id;
        this.name = name;
        this.children = new ArrayList<GraphNode>();
        this.bounds = new Rect();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Point getPositions() {
        return new Point(bounds.left, bounds.top);
    }

    public void setPositions(Point positions) {
        this.bounds.offsetTo(positions.x, positions.y);
    }

    public Rect getBounds() {
        return bounds;
    }

    public void setBounds(Rect bounds) {
        this.bounds = bounds;
    }

    public GraphNode getParent() {
        return parent;
    }

    public void setParent(GraphNode parent) {
        this.parent = parent;
    }

    public void addChild(GraphNode node){
        this.children.add(node);
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public ArrayList<GraphNode> getChildren(){
        return this.children;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GraphNode graphNode = (GraphNode) o;

        if (id != graphNode.id) return false;
        if (!name.equals(graphNode.name)) return false;
        if (!bounds.equals(graphNode.bounds)) return false;

        return true;
    }

    @Override
    public String toString() {
        return "GraphNode{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", positions=" + bounds +
                '}';
    }
}
