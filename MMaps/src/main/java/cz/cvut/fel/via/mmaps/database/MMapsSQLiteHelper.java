package cz.cvut.fel.via.mmaps.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by tomas on 4.12.13.
 */
public class MMapsSQLiteHelper extends SQLiteOpenHelper {

    public static final String TABLE_USERS = "users";
    public static final String COLUMN_USER_ID = "id_user";
    public static final String COLUMN_USER_NAME = "name";
    public static final String COLUMN_USER_PASSWORD = "password";

    public static final String TABLE_MAPS = "maps";
    public static final String COLUMN_MAP_ID = "id_map";
    public static final String COLUMN_MAP_USER_ID = "id_user";
    public static final String COLUMN_MAP_NAME = "name";
    public static final String COLUMN_MAP_URL = "url";

    public static final String TABLE_NODES = "nodes";
    public static final String COLUMN_NODE_ID = "id_node";
    public static final String COLUMN_NODE_MAP_ID = "id_map";
    public static final String COLUMN_NODE_NAME = "name";
    public static final String COLUMN_NODE_TEXT = "node_text";
    public static final String COLUMN_NODE_PARENT_ID = "id_parent";
    public static final String COLUMN_NODE_URL = "url";

    private static final String DATABASE_NAME = "mmaps.db";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE_USERS = "create table "
            + TABLE_USERS + "("
            + COLUMN_USER_ID + " integer primary key autoincrement, "
            + COLUMN_USER_NAME + " text not null, "
            + COLUMN_USER_PASSWORD + " text not null);";

    private static final String DATABASE_CREATE_MAPS = "create table "
            + TABLE_MAPS + "("
            + COLUMN_MAP_ID + " integer primary key autoincrement, "
            + COLUMN_MAP_NAME + " text not null, "
            + COLUMN_MAP_USER_ID + " integer, "
            + COLUMN_MAP_URL + " text, "
            + "FOREIGN KEY("+COLUMN_MAP_USER_ID+") REFERENCES "+TABLE_USERS+"("+COLUMN_USER_ID+"));";

    private static final String DATABASE_CREATE_NODES = "create table "
            + TABLE_NODES + "("
            + COLUMN_NODE_ID + " integer primary key autoincrement, "
            + COLUMN_NODE_NAME + " text not null, "
            + COLUMN_NODE_TEXT + " text not null, "
            + COLUMN_NODE_MAP_ID + " integer, "
            + COLUMN_NODE_PARENT_ID+" integer, "
            + COLUMN_NODE_URL + " text, "
            + "FOREIGN KEY("+COLUMN_NODE_MAP_ID+") REFERENCES "+TABLE_MAPS+"("+COLUMN_MAP_ID+"), "
            + "FOREIGN KEY("+COLUMN_NODE_PARENT_ID+") REFERENCES "+TABLE_NODES+"("+COLUMN_NODE_ID+"));";

    public MMapsSQLiteHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println(DATABASE_CREATE_MAPS);
        System.out.println(DATABASE_CREATE_NODES);
        System.out.println(DATABASE_CREATE_USERS);
        db.execSQL(DATABASE_CREATE_USERS);
        db.execSQL(DATABASE_CREATE_MAPS);
        db.execSQL(DATABASE_CREATE_NODES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(MMapsSQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MAPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NODES);
        onCreate(db);
    }
}
