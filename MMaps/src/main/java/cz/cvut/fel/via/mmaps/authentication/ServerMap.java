package cz.cvut.fel.via.mmaps.authentication;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import cz.cvut.fel.via.mmaps.R;
import cz.cvut.fel.via.mmaps.WelcomeActivity;
import cz.cvut.fel.via.mmaps.connection.ServerConnectionManager;
import cz.cvut.fel.via.mmaps.database.MapDataSource;

/**
 * Created by tomas on 5.12.13.
 */
public final class ServerMap {

    private AuthenticationManager am;
    private User user;
    private static ServerMap serverMap;
    private static final String CREATE_ADDRESS = "/maps/create";
    private static final String LIST_ALL_ADDRESS = "/maps";
    private static final String MAP_ACCESS = "/maps/m/";
    private MapDataSource mapDataSource;

    public ServerMap(){
        am = AuthenticationManager.getInstance();
        user = am.getLoggedUser();
    }

    public static ServerMap getInstance(){
        if (serverMap==null){
            serverMap = new ServerMap();
        }
        return serverMap;
    }

    public String createMap(String name,int root) {
        try{
            ServerConnectionManager scm = ServerConnectionManager.getInstance();
            JSONStringer json = scm.prepareJSON();
            json.key("name").value(name);
            HttpResponse response = scm.makeAuthenticatedRequest(CREATE_ADDRESS,"POST",json);

            //HttpUriRequest put = scm.createPost(CREATE_ADDRESS, json.toString());
            //HttpResponse response = scm.makeRequest(put);

        Log.v("MapsManager", response.getStatusLine().getReasonPhrase());

        if (response.getStatusLine().getStatusCode() == 200){
            Log.i("map creation","map created");
            JSONObject obj = ServerConnectionManager.getJSONBody(response);
            String mapUrl = obj.getString("url");
            if(root == 1){
                json = scm.prepareJSON();
                json.key("name").value(name);
                json.key("root").value(1);
                int addressOffset = mapUrl.indexOf(MAP_ACCESS);
                String editedAddress = mapUrl.substring(addressOffset);
                HttpResponse res = scm.makeAuthenticatedRequest(editedAddress,"PUT",json);
                Log.v("Edit root map",res.getStatusLine().getReasonPhrase());
            }
            return mapUrl;
        }

        if (response.getStatusLine().getStatusCode() == 418)
            throw new AuthenticationException(WelcomeActivity.instance.getString(R.string.user_exists));

        throw new AuthenticationException(response.getStatusLine().getReasonPhrase());
    }catch(Exception e){
           e.printStackTrace();
           return null;
    }
    }

    public synchronized void getMaps(){
        try{

            ServerConnectionManager scm = ServerConnectionManager.getInstance();
            HttpResponse response = scm.makeAuthenticatedRequest(LIST_ALL_ADDRESS,"GET",null);

            Log.v("MAPSManager", response.getStatusLine().getReasonPhrase());

            if (response.getStatusLine().getStatusCode() == 200){
                Log.i("map listing","map created");
                JSONObject obj = ServerConnectionManager.getJSONBody(response);
                JSONArray mapsData = obj.getJSONArray("data");
                for(int i=0;i<mapsData.length();i++){
                    JSONObject mD = (JSONObject) mapsData.get(i);
                    String url = mD.getString("url");
                    String name = mD.getString("name");
                    if(!mD.isNull("root")){
                        if (mD.getInt("root")==1){
                            mapDataSource.createMap(name,url);
                        }
                    }
                }
                System.out.println(mapsData);
            }
            if (response.getStatusLine().getStatusCode() == 418)
                throw new AuthenticationException(WelcomeActivity.instance.getString(R.string.user_exists));

            throw new AuthenticationException(response.getStatusLine().getReasonPhrase());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void setMapDataSource(MapDataSource mapDataSource) {
        this.mapDataSource = mapDataSource;
    }
}


