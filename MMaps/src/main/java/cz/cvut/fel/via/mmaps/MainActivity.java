package cz.cvut.fel.via.mmaps;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import cz.cvut.fel.via.mmaps.authentication.AuthenticationException;
import cz.cvut.fel.via.mmaps.authentication.AuthenticationManager;
import cz.cvut.fel.via.mmaps.authentication.ServerMap;
import cz.cvut.fel.via.mmaps.database.LocalUserDataSource;
import cz.cvut.fel.via.mmaps.database.Map;
import cz.cvut.fel.via.mmaps.database.MapDataSource;
import cz.cvut.fel.via.mmaps.database.Node;
import cz.cvut.fel.via.mmaps.database.NodeDataSource;
import cz.cvut.fel.via.mmaps.utils.CustomDrawableView;
import cz.cvut.fel.via.mmaps.utils.DeleteNodeDialog;
import cz.cvut.fel.via.mmaps.utils.GraphNode;
import cz.cvut.fel.via.mmaps.utils.MapDialog;
import cz.cvut.fel.via.mmaps.utils.NewMapDialog;
import cz.cvut.fel.via.mmaps.utils.NewNodeDialog;
import cz.cvut.fel.via.mmaps.utils.NodeDialog;
import cz.cvut.fel.via.mmaps.utils.NodeEntryDialog;
import cz.cvut.fel.via.mmaps.utils.UserDialog;


public class MainActivity extends Activity implements NewNodeDialog.NewNodeDialogListener,
                                                      NewMapDialog.NewMapDialogListener,
        DeleteNodeDialog.DeleteNodeDialogListener,MapDialog.MapDialogListener,
                                                      NodeEntryDialog.NodeEntryDialogListener{

    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mMapTitles;
    private CustomDrawableView mcd;

    private MapDataSource maps;
    private LocalUserDataSource users;
    private NodeDataSource nodes;

    private AuthenticationManager am;
    private String newMap;
    private Map newM;
    private String newNodeName;
    private Node newNode;
    private Map currentMap;
    private ArrayList<Map> allMaps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = mDrawerTitle = getTitle();
        mMapTitles = getResources().getStringArray(R.array.user_actions);//shapes_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mMapTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        am = AuthenticationManager.getInstance();

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        users = new LocalUserDataSource(this);
        users.open();

        maps = new MapDataSource(this);
        maps.open();

        nodes = new NodeDataSource(this);
        nodes.open();

        LinearLayout ll = (LinearLayout)findViewById(R.id.main_window);
        mcd = new CustomDrawableView(this,getFragmentManager(),nodes);
        GraphNode root = GraphNode.createGraph();

        if (am.isUserLogged()){
            //mcd.drawGraph(root);
            ll.addView(mcd);
            ServerMapHandler smh = new ServerMapHandler();
            smh.execute(new String[]{"0"});
            allMaps = (ArrayList<Map>) maps.getAllMaps();
            if(allMaps.size()>0){
                MapDialog md = new MapDialog(this,maps);
                md.show(this.getFragmentManager(),"maps");
            }
        }

        if (savedInstanceState == null) {
            /*getFragmentManager().beginTransaction()
                    .add(R.id.map, new PlaceholderFragment())
                    .commit();*/
        }
    }




    private class ServerMapHandler extends AsyncTask<String,Void,Object>{
        String url;

        @Override
        protected Object doInBackground(String... params) {
            int choice = Integer.valueOf(params[0]);
            ServerMap sm = new ServerMap();
            switch(choice){
                case 0:
                    sm.setMapDataSource(maps);
                    sm.getMaps();
                    break;
                case 1:
                    url = sm.createMap(params[1],1);
                    break;
                case 2:
                    url = sm.createMap(params[1],0);
                    break;

            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            if(newM!=null && url!=null){
                maps.updateMap(newM,url);
                mcd.clearScreen();
                mcd.drawGraph(maps.buildGraphFromMapTable(newM.getId()));
                currentMap = newM;
                newM = null;
            }
            if(newNode!=null && url!=null && currentMap != null){
                maps.updateMap(newNode,url);
                mcd.clearScreen();
                mcd.drawGraph(maps.buildGraphFromMapTable(currentMap.getId()));
                newNode = null;
            }

        }
    }

    private class LogoutHandler extends AsyncTask<Void,Void,Object>{

        @Override
        protected Object doInBackground(Void... voids) {
            try {
                AuthenticationManager.getInstance().logoutUser();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (AuthenticationException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o){
            finish();
            Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
            startActivity(intent);
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    @Override
    public void onDialogClick(MapDialog dialog) {
        long mapId = dialog.getMapId();
        currentMap = maps.getMap(mapId);
        mcd.clearScreen();
        mcd.drawGraph(maps.buildGraphFromMapTable(mapId));
    }

    @Override
    public void onDialogPositiveClick(NewMapDialog dialog) {
        newMap = dialog.getNewName();
        if(newMap!=null && am.getLoggedLocalUser()!=null){
            newM = maps.createMap(newMap, am.getLoggedLocalUser().getId());
            ServerMapHandler smh = new ServerMapHandler();
            smh.execute(new String[]{"1",newMap});
        }
    }
    @Override
    public void onDialogNegativeClick(NewMapDialog dialog){
        newMap = null;
    }

    @Override
    public void onDialogPositiveClick(NewNodeDialog dialog) {
        newNodeName = dialog.getNewName();
        long parentId = dialog.getParentId();
        if(currentMap != null && newNodeName != null){
            newNode = nodes.createNode(newNodeName,"",currentMap.getId(),parentId);
            ServerMapHandler smh = new ServerMapHandler();
            smh.execute(new String[]{"2",newNodeName});
        }
    }

    @Override
    public void onDialogNegativeClick(NewNodeDialog dialog) {

    }


    @Override
    public void onDialogPositiveClick(DeleteNodeDialog dialog) {
        long nodeId = dialog.getParentId();
        nodes.deleteNode(nodes.getNode(nodeId));
        mcd.clearScreen();
        mcd.drawGraph(maps.buildGraphFromMapTable(currentMap.getId()));
    }

    @Override
    public void onDialogNegativeClick(DeleteNodeDialog dialog) {

    }


    @Override
    public void onDialogPositiveClick(NodeEntryDialog dialog) {
        String text = dialog.getNewText();
        long id = dialog.getParentId();
        Node nN = nodes.getNode(id);
        nN.setText(text);
        nodes.updateNode(nN);
    }

    @Override
    public void onDialogNegativeClick(NodeEntryDialog dialog) {

    }

    private void selectItem(int position) {
        switch (position){
            case 0:
                int noOfMaps;
                if (am.getLoggedLocalUser()!=null){
                    noOfMaps = maps.getMapsCount(am.getLoggedLocalUser().getId());
                }else{
                    noOfMaps = -1;
                }
                UserDialog ud = new UserDialog(noOfMaps);
                ud.show(this.getFragmentManager(),"Neco");
                break;
            case 3:
                LogoutHandler logout = new LogoutHandler();
                logout.execute();
                break;
            case 1:
                NewMapDialog nmd = new NewMapDialog();
                nmd.show(this.getFragmentManager(),"NewMapDialog");
                break;
            case 2:
                MapDialog md = new MapDialog(this,maps);
                md.show(this.getFragmentManager(),"MapsDialog");
                //mcd.invalidate();
                break;
        }
        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        //setTitle(mMapTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}