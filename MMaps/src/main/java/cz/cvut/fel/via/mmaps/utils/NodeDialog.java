package cz.cvut.fel.via.mmaps.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import cz.cvut.fel.via.mmaps.R;
import cz.cvut.fel.via.mmaps.database.NodeDataSource;

/**
 * Created by tomas on 10/28/13.
 */
public class NodeDialog extends DialogFragment {

    private NodeDataSource nodes;
    private FragmentManager fm;
    private Context context;
    private String title;
    private long parentId;

    public NodeDialog(Context context, FragmentManager fm,long parentId, String title,NodeDataSource nodes){
        this.context = context;
        this.fm = fm;
        this.parentId = parentId;
        this.title = title;
        this.nodes = nodes;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (this.title==null){
            builder.setTitle(R.string.dialog);
        }
        else{
            builder.setTitle(title);
        }
        builder.setItems(R.array.action_array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case 1:
                            NewNodeDialog nnd = new NewNodeDialog(parentId);
                            nnd.show(fm,"New node");
                            break;
                        case 3:
                            DeleteNodeDialog ndd = new DeleteNodeDialog(parentId);
                            ndd.show(fm,"NodeDelete");
                            break;
                        case 0:
                            NodeEntryDialog ne = new NodeEntryDialog(parentId,nodes.getNode(parentId).getText(),false);
                            ne.show(fm,"NodeEntry");
                            break;
                        case 2:
                            NodeEntryDialog ne2 = new NodeEntryDialog(parentId,nodes.getNode(parentId).getText(),true);
                            ne2.show(fm,"NodeEntry");
                            break;

                    }
                    //Toast.makeText(context,"Pokus "+which,Toast.LENGTH_SHORT).show();
                }
            });
        return builder.create();
    }

    public NodeDialog(String title){
        this.title = title;
    }
}
