package cz.cvut.fel.via.mmaps.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import cz.cvut.fel.via.mmaps.R;
import cz.cvut.fel.via.mmaps.database.Map;
import cz.cvut.fel.via.mmaps.database.MapDataSource;

/**
 * Created by tomas on 10/28/13.
 */
public class MapDialog extends DialogFragment {

    private MapDataSource maps;
    private FragmentManager fm;
    private Context context;
    private long mapId;
    private String title;
    private long parentId;

    public MapDialog(Context context,MapDataSource maps){
        this.context = context;
        this.fm = fm;
        this.maps = maps;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (this.title==null){
            builder.setTitle(R.string.chooseMap);
        }
        final ArrayList<Map> m = (ArrayList<Map>) maps.getAllMaps();
        final String[] titles = new String[m.size()];
        for(int i=0;i<m.size();i++){
            titles[i] = m.get(i).getName();
        }
        builder.setItems(titles, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    mapId = m.get(which).getId();
                    Toast.makeText(context, "Map "+ titles[which]+" set as active", Toast.LENGTH_SHORT).show();
                    mListener.onDialogClick(MapDialog.this);

                }
            });
        return builder.create();
    }

    public interface MapDialogListener{
        public void onDialogClick(MapDialog dialog);
    }

    public long getMapId(){
        return this.mapId;
    }

    public MapDialog(String title){
        this.title = title;
    }

    MapDialogListener mListener;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            mListener = (MapDialogListener) activity;
        }catch(ClassCastException e){
            Log.e("chyba pri prenosu z dialogu","chyba pri prenosu z dialogu");
        }
    }
}
