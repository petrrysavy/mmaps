package cz.cvut.fel.via.mmaps.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.widget.EditText;

import cz.cvut.fel.via.mmaps.R;

/**
 * Created by tomas on 10/28/13.
 */
public class NewMapDialog extends DialogFragment {

    private String value;
    private EditText input = null;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Create new map");
        builder.setMessage("Input the name of new map:");
        input = new EditText(getActivity().getApplicationContext());
        input.setTextColor(Color.parseColor("#000000"));
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog,int whichButton){
                mListener.onDialogPositiveClick(NewMapDialog.this);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
              public void onClick(DialogInterface dialog,int whichButton){
                mListener.onDialogNegativeClick(NewMapDialog.this);
              }
        });
        return builder.create();
    }

    public NewMapDialog(){
    }

    public String getNewName(){
        value = input.getText().toString();
        return value;
    }

    public interface NewMapDialogListener{
        public void onDialogPositiveClick(NewMapDialog dialog);
        public void onDialogNegativeClick(NewMapDialog dialog);
    }

    NewMapDialogListener mListener;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            mListener = (NewMapDialogListener) activity;
        }catch(ClassCastException e){
            Log.e("chyba pri prenosu z dialogu","chyba pri prenosu z dialogu");
        }
    }

}
