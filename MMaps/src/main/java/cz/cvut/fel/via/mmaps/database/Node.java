package cz.cvut.fel.via.mmaps.database;

/**
 * Created by tomas on 5.12.13.
 */
public class Node {

    private long id;
    private String name;
    private String text;
    private long map_id;
    private long parent_id;
    private String url;

    public long getParent_id() {
        return parent_id;
    }

    public void setParent_id(long parent_id) {
        this.parent_id = parent_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getMap_id() {
        return map_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setMap_id(long map_id) {
        this.map_id = map_id;
    }

    @Override
    public String toString() {
        return "Node{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", text='" + text + '\'' +
                ", map_id=" + map_id +
                '}';
    }
}
