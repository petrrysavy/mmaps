package cz.cvut.fel.via.mmaps.database;

import cz.cvut.fel.via.mmaps.utils.GraphNode;

/**
 * Created by tomas on 4.12.13.
 */
public class Map {

    private long id;
    private String name;
    private long user_id;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Map{" +
                "name='" + name + '\'' +
                ", user_id=" + user_id +
                '}';
    }
}
