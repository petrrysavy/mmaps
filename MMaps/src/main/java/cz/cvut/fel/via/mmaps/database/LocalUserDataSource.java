package cz.cvut.fel.via.mmaps.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomas on 5.12.13.
 */
public class LocalUserDataSource {

    private SQLiteDatabase db;
    private MMapsSQLiteHelper dbHelper;
    private  String[] allColumns = {MMapsSQLiteHelper.COLUMN_USER_ID,
                                    MMapsSQLiteHelper.COLUMN_USER_NAME,
                                    MMapsSQLiteHelper.COLUMN_USER_PASSWORD};

    public LocalUserDataSource(Context context){
        dbHelper = new MMapsSQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void upgradeDatabase(){
        dbHelper.onUpgrade(db,0,1);
    }

    public void close() {
        dbHelper.close();
    }

    public LocalUser createUser(String name,String password){
        LocalUser tryLocal = getUser(name,password);
        if (tryLocal!=null){
            return tryLocal;
        }
        ContentValues values = new ContentValues();
        values.put(MMapsSQLiteHelper.COLUMN_USER_NAME,name);
        values.put(MMapsSQLiteHelper.COLUMN_USER_PASSWORD,password);
        long insertID = db.insert(MMapsSQLiteHelper.TABLE_USERS,null,values);
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_USERS,allColumns,MMapsSQLiteHelper.COLUMN_USER_ID + " = " + insertID,null,null,null,null);
        cursor.moveToFirst();
        LocalUser newLocalUser = cursorToUser(cursor);
        cursor.close();
        return newLocalUser;
    }

    public LocalUser getUser(String name,String password){
        ContentValues values = new ContentValues();
        values.put(MMapsSQLiteHelper.COLUMN_USER_NAME,name);
        values.put(MMapsSQLiteHelper.COLUMN_USER_PASSWORD,password);
        //long insertID = db.insert(MMapsSQLiteHelper.TABLE_USERS,null,values);
        Cursor u = db.query(MMapsSQLiteHelper.TABLE_USERS,
                allColumns, "name=? and password=?", new String[]{name, password},
                null, null, null);
        u.moveToFirst();
        //Cursor u = db.rawQuery("SELECT id_user,name FROM users WHERE" +
        //                        " name=\""+name+"\" and password=\""+password+"\";" ,null);
        if(u!=null && u.getCount()>0){
            return cursorToUser(u);
        }else{
            return null;
        }
    }

    public void deleteUser(LocalUser localUser){
        long id = localUser.getId();
        System.out.println("Deleting user with id: "+id);
        db.delete(MMapsSQLiteHelper.TABLE_USERS,MMapsSQLiteHelper.COLUMN_USER_ID + " = " + id,null);
    }

    public List<LocalUser> getAllUsers() {
        List<LocalUser> localUsers = new ArrayList<LocalUser>();
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_USERS,allColumns,null,null,null,null,null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            LocalUser localUser = cursorToUser(cursor);
            localUsers.add(localUser);
            cursor.moveToNext();
        }
        cursor.close();
        return localUsers;
    }

    private LocalUser cursorToUser(Cursor cursor){
        LocalUser localUser = new LocalUser();
        localUser.setId(cursor.getLong(0));
        localUser.setName(cursor.getString(1));
        return localUser;
    }
}
