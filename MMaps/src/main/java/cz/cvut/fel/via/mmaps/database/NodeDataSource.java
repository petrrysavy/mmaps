package cz.cvut.fel.via.mmaps.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomas on 5.12.13.
 */
public class NodeDataSource {

    private SQLiteDatabase db;
    private MMapsSQLiteHelper dbHelper;
    private  String[] allColumns = {MMapsSQLiteHelper.COLUMN_NODE_ID,MMapsSQLiteHelper.COLUMN_NODE_NAME,
                                    MMapsSQLiteHelper.COLUMN_NODE_TEXT,MMapsSQLiteHelper.COLUMN_NODE_MAP_ID,
                                    MMapsSQLiteHelper.COLUMN_NODE_PARENT_ID};

    public NodeDataSource(Context context){
        dbHelper = new MMapsSQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Node createNode(String name,String text,long mapId,long parentId){
        ContentValues values = new ContentValues();
        values.put(MMapsSQLiteHelper.COLUMN_NODE_NAME,name);
        values.put(MMapsSQLiteHelper.COLUMN_NODE_TEXT,text);
        values.put(MMapsSQLiteHelper.COLUMN_NODE_MAP_ID,mapId);
        values.put(MMapsSQLiteHelper.COLUMN_NODE_PARENT_ID,parentId);

        long insertID = db.insert(MMapsSQLiteHelper.TABLE_NODES,null,values);
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_NODES,allColumns,MMapsSQLiteHelper.COLUMN_NODE_ID + " = " + insertID,null,null,null,null);
        cursor.moveToFirst();
        Node newNode = cursorToNode(cursor);
        cursor.close();
        return newNode;
    }

    public Node getNode(long nodeId){
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_NODES,allColumns,
                MMapsSQLiteHelper.COLUMN_NODE_ID + " = " + nodeId,null,null,null,null);
        cursor.moveToFirst();
        Node newNode = cursorToNode(cursor);
        cursor.close();
        return newNode;
    }

    public List<Node> getNodes(long mapId, long parentId){
        List<Node> nodes = new ArrayList<Node>();
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_NODES,allColumns,
                MMapsSQLiteHelper.COLUMN_NODE_MAP_ID+"=? and "
               +MMapsSQLiteHelper.COLUMN_NODE_PARENT_ID+"=?",
                new String[]{mapId+"",parentId+""},null,null,null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            Node node = cursorToNode(cursor);
            nodes.add(node);
            cursor.moveToNext();
        }
        cursor.close();
        return nodes;
    }

    public int updateNode(Node node){
        ContentValues values = new ContentValues();
        values.put(MMapsSQLiteHelper.COLUMN_NODE_NAME,node.getName());
        values.put(MMapsSQLiteHelper.COLUMN_NODE_TEXT,node.getText());
        values.put(MMapsSQLiteHelper.COLUMN_NODE_MAP_ID,node.getMap_id());
        values.put(MMapsSQLiteHelper.COLUMN_NODE_PARENT_ID,node.getParent_id());

        int updated = db.update(MMapsSQLiteHelper.TABLE_NODES,values,MMapsSQLiteHelper.COLUMN_NODE_ID + "=?",new String[]{node.getId()+""});
        return updated;
    }

    public void deleteNode(Node node){
        long id = node.getId();
        System.out.println("Deleting node with id: "+id);
        db.delete(MMapsSQLiteHelper.TABLE_NODES,MMapsSQLiteHelper.COLUMN_NODE_ID + " = " + id,null);
    }

    public List<Node> getAllNodes() {
        List<Node> nodes = new ArrayList<Node>();
        Cursor cursor = db.query(MMapsSQLiteHelper.TABLE_NODES,allColumns,null,null,null,null,null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()){
            Node node = cursorToNode(cursor);
            nodes.add(node);
            cursor.moveToNext();
        }
        cursor.close();
        return nodes;
    }

    private Node cursorToNode(Cursor cursor){
        Node node = new Node();
        node.setId(cursor.getLong(0));
        node.setName(cursor.getString(1));
        node.setText(cursor.getString(2));
        node.setMap_id(cursor.getLong(3));
        node.setParent_id(cursor.getLong(4));
        if(!cursor.isNull(5)){
            node.setUrl(cursor.getString(5));
        }
        return node;
    }
}
