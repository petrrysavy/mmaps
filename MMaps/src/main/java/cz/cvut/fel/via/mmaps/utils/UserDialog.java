package cz.cvut.fel.via.mmaps.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.DialogPreference;

import cz.cvut.fel.via.mmaps.R;
import cz.cvut.fel.via.mmaps.authentication.AuthenticationManager;

/**
 * Created by tomas on 10/28/13.
 */
public class UserDialog extends DialogFragment {

    private String title;
    private int count;
    private AuthenticationManager am = AuthenticationManager.getInstance();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if(am.getLoggedLocalUser()!=null){
        builder.setTitle(am.getLoggedLocalUser().getName())
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                })
                .setMessage("You user: "+am.getLoggedLocalUser().getName()
                        +"\nhave created so far "+count+" mind maps."
                        +"Congratulations! :-)");
        }else{
            builder.setTitle("Nothing to display")
                    .setCancelable(false)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    })
                    .setMessage("You are not logged in...");
        }
        return builder.create();
    }

    public UserDialog(int count){
        this.count = count;
    }
}
