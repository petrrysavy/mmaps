package cz.cvut.fel.via.mmaps.authentication;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;

import cz.cvut.fel.via.mmaps.R;
import cz.cvut.fel.via.mmaps.WelcomeActivity;
import cz.cvut.fel.via.mmaps.connection.ServerConnectionManager;
import cz.cvut.fel.via.mmaps.database.LocalUserDataSource;
import cz.cvut.fel.via.mmaps.database.LocalUser;

/**
 * Singleton class that handles authentication.
 *
 * Created by Petr on 24.11.13.
 */
public final class AuthenticationManager {
    private static final String USERNAME_KEY = "username";
    private static final String PASSWORD_KEY = "password";
    private static final String SALT_KEY = "salt";
    private static final String TOKEN_KEY = "token";

    private static final String CREATE_ADDRESS = "/users/create";
    private static final String AUTH_SESSION_ADDRESS = "/auth/salt/";
    private static final String LOGIN_ADDRESS = "/auth/login/";
    private static final String LOGOUT_ADDRESS = "/auth/logout";

    private static AuthenticationManager instance;

    private static LocalUserDataSource users;
    private static Context context;

    private MessageDigest digest;
    /** The user that is currently logged. If no user is logged, then null. */
    private User user;
    private LocalUser localUser;

    private AuthenticationManager() {
        try {
            digest = MessageDigest.getInstance("sha-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Log.e("Authentication manager", "Could not calculate sha-256!!!", e);
        }

        loadLoggedUser();
    }

    /** Gets the singleton instance. */
    public static AuthenticationManager getInstance() {
        if(instance == null)
        instance = new AuthenticationManager();
        return instance;
    }

    /**
     * Creates new user with given credentials. Called automatically by the SignUp activity. After that logs the user in.
     * @param name Name of the user.
     * @param password His password.
     * @return User that was created.
     * @throws IOException When there is a problem with server-client communication.
     * @throws AuthenticationException Something wrong with teh credentials, maybe a used username.
     */
    public User createUser(String name, String password) throws IOException, AuthenticationException {
        if(user != null){
            throw new AuthenticationException("Log out the user first");
        }
        localUser = users.createUser(name,sha256(password));
        try {
            JSONStringer json = new JSONStringer().object()
                    .key(USERNAME_KEY).value(name)
                    .key(PASSWORD_KEY).value(password)
                    .endObject();

            ServerConnectionManager scm = ServerConnectionManager.getInstance();
            HttpUriRequest put = scm.createPut(CREATE_ADDRESS, json.toString());
            HttpResponse response = scm.makeRequest(put);

            Log.v("AuthenticationManager", response.getStatusLine().getReasonPhrase());

            if (response.getStatusLine().getStatusCode() == 200)
                return loginUser(name, password);

            if (response.getStatusLine().getStatusCode() == 418)
                throw new AuthenticationException(WelcomeActivity.instance.getString(R.string.user_exists));

            throw new AuthenticationException(response.getStatusLine().getReasonPhrase());
        } catch (JSONException e) {
            e.printStackTrace();
            throw new IOException("Error creating JSON.");
        }
    }

    /**
     * Returns the salt.
     *
     * @param name
     * @return salt
     */
    private String getSalt(String name) throws IOException, JSONException {
        ServerConnectionManager scm = ServerConnectionManager.getInstance();
        HttpUriRequest get = scm.createGet(AUTH_SESSION_ADDRESS + name);
        HttpResponse response = scm.makeRequest(get);

        JSONObject obj = ServerConnectionManager.getJSONBody(response);

        return obj.getString(SALT_KEY);
    }

    /** Returns the user that is logged into the application. */
    public User getLoggedUser() {
        return user;
    }

    public LocalUser getLoggedLocalUser() {return localUser;}

    /** Returns whether there is a logged user. */
    public boolean isUserLogged() {
        return user != null;
    }

    /**
     * Logs the current user out. Deletes the session from the server.
     * @throws JSONException Error using JSON.
     * @throws IOException Error on the line.
     * @throws AuthenticationException Something is wrong with the credentials.
     */
    public synchronized void logoutUser() throws JSONException, IOException, AuthenticationException {
        if(user == null)
            throw new AuthenticationException("There is no user logged into the application");

        deleteLoggedUser();

        user = null;
        localUser = null;

        /*Log.i("Authentication manager", response.getStatusLine().getReasonPhrase());
        BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        String line;
        while((line = br.readLine()) != null)
            Log.i("Authentication manager", line);
        br.close();*/
    }

    /**
     * Logs the user with the given credentials in.
     * @param name Username.
     * @param password His password..
     * @return The logged user.
     * @return User that was created.
     * @throws IOException When there is a problem with server-client communication.
     * @throws AuthenticationException Something wrong with teh credentials, maybe wrong password.
     */
    public synchronized User loginUser(String name, String password) throws IOException, JSONException, AuthenticationException {
        if(user != null)
            throw new AuthenticationException("Log out the user first");

        if (localUser==null){
            localUser = users.createUser(name,sha256(password));
        }

        String salt = getSalt(name);
        String mySalt = randomString(48);

        user = new User(name, password,salt);


        Log.i("Authentication manager", "salt "+ salt);

        ServerConnectionManager scm = ServerConnectionManager.getInstance();
        HttpUriRequest post = scm.createPost(LOGIN_ADDRESS + name,
                new JSONStringer().object()
                      .key(SALT_KEY).value(mySalt)
                      .key(PASSWORD_KEY).value(initialPasswordHash(password, salt, mySalt))
                      .endObject().toString());

        HttpResponse response = scm.makeRequest(post);

        if (response.getStatusLine().getStatusCode() != 200) {
            user = null;
            /*Log.i("Authentication manager", response.getStatusLine().getReasonPhrase());
            BufferedReader br = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            String line;
            while((line = br.readLine()) != null)
                Log.i("Authentication manager", line);
            br.close();*/
            throw new AuthenticationException("Failed to login. Password or username seems to be incorrect : "+response.getStatusLine().getReasonPhrase());
        }

        storeLoggedUser();

        return user;
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        AuthenticationManager.context = context;
        users = new LocalUserDataSource(context);
        users.open();
        // Deletes local database
        //users.upgradeDatabase();
    }


    public String initialPasswordHash(String password, String salt1, String salt2) {
        return sha256(sha256(password + salt1) + salt2);
    }

    public String sha256(String input) {
        try {

            digest.reset();
            digest.update(input.getBytes("utf-8"));
            byte[] byteData = digest.digest();

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
                        .substring(1));
            }

            return sb.toString();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private final void loadLoggedUser() {
        // this is really not safe approach, because any user with root privileges can access the
        // stored password (in very weakly encrypted version)
        // ... but in real, he could have logged the keyboard when typing password as is a root
        SharedPreferences sp = getSharedPreferences();
        user = User.loadCredentials(sp);
    }

    private SharedPreferences getSharedPreferences() {
        return WelcomeActivity.instance.getSharedPreferences("authentication_manager", Context.MODE_PRIVATE);
    }

    private void storeLoggedUser() {
        if (user == null)
            return;

        SharedPreferences sp = getSharedPreferences();
        user.storeCredential(sp);
    }

    private void deleteLoggedUser() {
        SharedPreferences sp = getSharedPreferences();
        User.clearCredentials(sp);
    }

    protected static String encode(String input) {
    // rot 13 encoding
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if ((c < 32) || (c > 126)) {
                sb.append(c);
            } else {
                c += 13;
                if (c > 126) {
                    c -= ((126 - 32) + 1);
                }
                sb.append(c);
            }
        }

        return sb.toString();
    }

    protected static String decode(String input) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if ((c < 32) || (c > 126)) {
                sb.append(c);
            } else {
                c -= 13;
                if (c < 32) {
                    c += ((126 - 32) + 1);
                }
                sb.append(c);
            }
        }

        return sb.toString();

    }
    
    private final char[] values = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                                    't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '-' };
    
    private String randomString(int len)
    {
        Random r = new Random();
        char[] string = new char[len];
        for(int i = 0; i < len; i++)
            string[i] = values[r.nextInt(values.length)];
        return new String(string);
    }
}
