package cz.cvut.fel.via.mmaps.utils.graphBuilder;

import android.graphics.Paint;
import android.graphics.Rect;

import cz.cvut.fel.via.mmaps.utils.GraphNode;

/**
 * Created by Petr Ryšavý on 5.11.13.
 */
public class GraphBuilder {

    public static final int HORIZONTAL_PADDING = 20;
    public static final int VERTICAL_PADDING = 15;
    public static final int NODE_HORIZONTAL_DIST = 30;
    private static final int NODE_VERTICAL_DIST = 10;
    private final Paint paint;

    public GraphBuilder(Paint paint)
    {
        this.paint = paint;
    }

    public void buildGraph(GraphNode root)
    {
        buildSubgraph(root, 0, 0);
    }

    private Rect buildSubgraph(GraphNode root, int x, int y)
    {
        final Rect bounds = new Rect();
        final String text = root.getName();
        paint.getTextBounds(text, 0, text.length(), bounds);

        final Rect nodeBounds = new Rect(x, y, x + bounds.width() + 2*HORIZONTAL_PADDING, y + bounds.height()+2*VERTICAL_PADDING);
        root.setBounds(nodeBounds);

        if(root.getChildren().size() == 0)
            return nodeBounds;

        int subgraphX = nodeBounds.right + NODE_HORIZONTAL_DIST;
        int subgraphY = y;
        int width = nodeBounds.width();

        for(GraphNode node : root.getChildren())
        {
            final Rect subgraphBounds = buildSubgraph(node, subgraphX, subgraphY);
            subgraphY += subgraphBounds.height() + VERTICAL_PADDING;
            width = Math.max(width, nodeBounds.width() + NODE_HORIZONTAL_DIST + subgraphBounds.width());
        }

        subgraphY -= VERTICAL_PADDING;

        return new Rect(x, y, x + width, subgraphY);
    }

}
