package cz.cvut.fel.via.mmaps.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by tomas on 10/28/13.
 */
public class NodeEntryDialog extends DialogFragment {

    private String value;
    private String text;
    private boolean edit;
    private EditText input = null;
    private long parentId;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if(edit){
            builder.setTitle("Edit node");
            builder.setMessage("Input the text for node:");
            input = new EditText(getActivity().getApplicationContext());
            input.setText(text);
            input.setTextColor(Color.parseColor("#000000"));
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
            input.setLayoutParams(lp);
            builder.setView(input);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    mListener.onDialogPositiveClick(NodeEntryDialog.this);
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
              public void onClick(DialogInterface dialog,int whichButton){
                mListener.onDialogNegativeClick(NodeEntryDialog.this);
              }
            });
        }else{
            builder.setTitle("Show node");
            builder.setMessage(text);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }
            });
        }
        
        return builder.create();
    }

    public NodeEntryDialog(long parentId,String text,boolean edit){
        this.parentId = parentId;
        this.text = text;
        this.edit = edit;
    }

    public String getNewText(){
        value = input.getText().toString();
        return value;
    }

    public long getParentId(){
        return parentId;
    }

    public interface NodeEntryDialogListener{
        public void onDialogPositiveClick(NodeEntryDialog dialog);
        public void onDialogNegativeClick(NodeEntryDialog dialog);
    }

    NodeEntryDialogListener mListener;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            mListener = (NodeEntryDialogListener) activity;
        }catch(ClassCastException e){
            Log.e("chyba pri prenosu z dialogu","chyba pri prenosu z dialogu");
        }
    }

}
