package cz.cvut.fel.via.mmaps.authentication;

/**
 * Created by Petr on 24.11.13.
 */
public class AuthenticationException extends Exception {
    public AuthenticationException(String detailMessage) {
        super(detailMessage);
    }
}
