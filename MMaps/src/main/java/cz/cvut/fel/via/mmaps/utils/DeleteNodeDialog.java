package cz.cvut.fel.via.mmaps.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;

/**
 * Created by tomas on 10/28/13.
 */
public class DeleteNodeDialog extends DialogFragment {

    private String value;
    private EditText input = null;
    private long parentId;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Delete node");
        builder.setMessage("Really delete node?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
            mListener.onDialogPositiveClick(DeleteNodeDialog.this);
           }
            });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){
              public void onClick(DialogInterface dialog,int whichButton){
                mListener.onDialogNegativeClick(DeleteNodeDialog.this);
              }
            });
        return builder.create();
    }

    public DeleteNodeDialog(long parentId){
        this.parentId = parentId;
    }

    public String getNewText(){
        value = input.getText().toString();
        return value;
    }

    public long getParentId(){
        return parentId;
    }

    public interface DeleteNodeDialogListener{
        public void onDialogPositiveClick(DeleteNodeDialog dialog);
        public void onDialogNegativeClick(DeleteNodeDialog dialog);
    }

    DeleteNodeDialogListener mListener;

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            mListener = (DeleteNodeDialogListener) activity;
        }catch(ClassCastException e){
            Log.e("chyba pri prenosu z dialogu","chyba pri prenosu z dialogu");
        }
    }

}
